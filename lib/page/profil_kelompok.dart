import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:project_mkh/page/about_page.dart';

class ProfilKelompok extends StatelessWidget {
  const ProfilKelompok({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        elevation: 10,
        backgroundColor: const Color.fromARGB(255, 3, 113, 102),
        title: Text(
          'Profil Kelompok',
          style: GoogleFonts.poppins(
              fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white),
        ),
      ),
      body: ListView(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //berliana putri
              Container(
                margin: const EdgeInsets.all(15),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    elevation: 10,
                    //membuat elemen foto
                    child: Row(
                      children: [
                        Container(
                          width: 150,
                          height: double.infinity,
                          decoration: const BoxDecoration(
                              color: Colors.grey,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  bottomLeft: Radius.circular(20)),
                              image: DecorationImage(
                                  image: AssetImage('images/berliana.jpeg'),
                                  fit: BoxFit.contain)),
                        ),
                        Column(
                          children: [
                            //membuat nama dan biodata = DAVID RIYAN KURNIAWAN
                            Container(
                              margin: const EdgeInsets.fromLTRB(40, 10, 0, 0),
                              child: Text(
                                'BERLIANA PUTRI',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 15, fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(40, 0, 0, 0),
                              child: Text(
                                'SEPTI YANTI',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 15, fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Center(
                                child: Text(
                                  'I live in Tulang Bawang Barat, Lampung',
                                  maxLines: 3,
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(
                                      fontSize: 10,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              //dafa nabila
              Container(
                margin: const EdgeInsets.all(15),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    elevation: 10,
                    //membuat elemen foto
                    child: Row(
                      children: [
                        Container(
                          width: 150,
                          height: double.infinity,
                          decoration: const BoxDecoration(
                              color: Colors.grey,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  bottomLeft: Radius.circular(20)),
                              image: DecorationImage(
                                  image: AssetImage('images/dafa.jpeg'),
                                  fit: BoxFit.contain)),
                        ),
                        Column(
                          children: [
                            //membuat nama dan biodata = DAVID RIYAN KURNIAWAN
                            Container(
                              margin: const EdgeInsets.fromLTRB(40, 10, 0, 0),
                              child: Text(
                                'DAFA NABILA',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 15, fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(40, 0, 0, 0),
                              child: Text(
                                'ROSA',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 15, fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Center(
                                child: Text(
                                  'I live in Tulang Bawang Barat, Lampung',
                                  maxLines: 3,
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(
                                      fontSize: 10,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              //david riyan
              Container(
                margin: const EdgeInsets.all(15),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    elevation: 10,
                    //membuat elemen foto
                    child: Row(
                      children: [
                        Container(
                          width: 150,
                          height: double.infinity,
                          decoration: const BoxDecoration(
                              color: Colors.grey,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  bottomLeft: Radius.circular(20)),
                              image: DecorationImage(
                                  image: AssetImage('images/david.jpg'),
                                  fit: BoxFit.contain)),
                        ),
                        Column(
                          children: [
                            //membuat nama dan biodata = DAVID RIYAN KURNIAWAN
                            Container(
                              margin: const EdgeInsets.fromLTRB(40, 10, 0, 0),
                              child: Text(
                                'DAVID RIYAN',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 15, fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(40, 0, 0, 0),
                              child: Text(
                                'KURNIAWAN',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 15, fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Center(
                                child: Text(
                                  'I live in Lampung Timur, Lampung',
                                  maxLines: 2,
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(
                                      fontSize: 10,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              //marliyus
              Container(
                margin: const EdgeInsets.all(15),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    elevation: 10,
                    //membuat elemen foto
                    child: Row(
                      children: [
                        Container(
                          width: 150,
                          height: double.infinity,
                          decoration: const BoxDecoration(
                              color: Colors.grey,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  bottomLeft: Radius.circular(20)),
                              image: DecorationImage(
                                  image: AssetImage('images/marliyus.jpeg'),
                                  fit: BoxFit.contain)),
                        ),
                        Column(
                          children: [
                            //membuat nama dan biodata = DAVID RIYAN KURNIAWAN
                            Container(
                              margin: const EdgeInsets.fromLTRB(40, 10, 0, 0),
                              child: Text(
                                'MARLIYUS JAYA ',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 15, fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(40, 0, 0, 0),
                              child: Text(
                                'PUTRA',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 15, fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Text(
                                'I live in Way Kanan, Lampung',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 10, fontWeight: FontWeight.w500),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              //nur afti
              Container(
                margin: const EdgeInsets.all(15),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    elevation: 10,
                    //membuat elemen foto
                    child: Row(
                      children: [
                        Container(
                          width: 150,
                          height: double.infinity,
                          decoration: const BoxDecoration(
                              color: Colors.grey,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  bottomLeft: Radius.circular(20)),
                              image: DecorationImage(
                                  image: AssetImage('images/nur.jpeg'),
                                  fit: BoxFit.contain)),
                        ),
                        Column(
                          children: [
                            //membuat nama dan biodata = nur afti
                            Container(
                              margin: const EdgeInsets.fromLTRB(40, 10, 0, 0),
                              child: Text(
                                'NUR AFTI QOMALA',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 15, fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(40, 0, 0, 0),
                              child: Text(
                                'DINI',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 15, fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Text(
                                'I live in Lampung Selatan, Lampung',
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize: 10, fontWeight: FontWeight.w500),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
