import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:project_mkh/page/main_page.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(25, 50, 0, 10),
            child: Text(
              'Apl of the week',
              style: GoogleFonts.pompiere(
                  decoration: TextDecoration.underline,
                  decorationColor: Colors.green,
                  decorationThickness: 1,
                  fontFeatures: [const FontFeature.enable('abvs')],
                  fontSize: 30,
                  fontWeight: FontWeight.w600),
              textAlign: TextAlign.left,
            ),
          ),
          Center(
            child: Container(
              width: 280,
              height: 280,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('images/uinril.png'),
                      fit: BoxFit.contain)),
            ),
          ),
          Container(
            child: Align(
              alignment: const Alignment(-0.88, 0),
              child: Text(
                'TREES IN',
                maxLines: 2,
                style: GoogleFonts.poppins(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 30),
              ),
            ),
          ),
          Container(
            child: Align(
              alignment: Alignment.center,
              child: Text(
                'UIN RADEN INTAN',
                style: GoogleFonts.poppins(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 30),
              ),
            ),
          ),
          Container(
            child: Align(
              alignment: const Alignment(0.80, 0),
              child: Text(
                'LAMPUNG',
                maxLines: 2,
                style: GoogleFonts.poppins(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 30),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(25, 5, 0, 0),
            child: Text(
              'this application for project',
              style: GoogleFonts.poppins(color: Colors.black, fontSize: 15),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(25, 0, 0, 0),
            child: Text(
              'management green campus lesson',
              style: GoogleFonts.poppins(color: Colors.black, fontSize: 15),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(25, 0, 0, 0),
            child: Text(
              'so, enjoy with this application',
              style: GoogleFonts.poppins(color: Colors.black, fontSize: 15),
            ),
          ),
          Align(
            alignment: const Alignment(0.80, 0),
            child: Material(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(20),
              elevation: 25,
              child: Container(
                margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                width: 150,
                height: 60,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    gradient: const LinearGradient(
                        colors: [Colors.teal, Colors.green, Colors.teal],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight)),
                child: Material(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.transparent,
                  child: InkWell(
                    splashColor: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    onTap: () {
                      Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (context) {
                          return const MainPage();
                        },
                      ));
                    },
                    child: Center(
                        child: Text(
                      'Get Started',
                      style: GoogleFonts.poppins(
                          fontSize: 19,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    )),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
