import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_flutter/qr_flutter.dart';

class JambuPage extends StatelessWidget {
  const JambuPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        elevation: 10,
        backgroundColor: Color.fromARGB(255, 3, 113, 102),
        title: Text(
          'JAMBU',
          style: GoogleFonts.poppins(
              fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white),
        ),
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Text(
                'Jambu biji perdu atau pohon kecil, tinggi 2-10 m, percabangan banyak. Batangnya berkayu, keras, kulit batang licin, mengelupas, berwarna cokelat kehijauan. Daun tunggal, bertangkai pendek, letak berhadapan, daun muda berambut halus, permukaan atas daun tua licin.',
                style: TextStyle(fontSize: 15),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Center(
                  child: QrImage(
                    size: 300,
                    version: 6,
                    backgroundColor: Colors.grey,
                    foregroundColor: Colors.black,
                    errorCorrectionLevel: QrErrorCorrectLevel.M,
                    padding: EdgeInsets.all(30),
                    data: 'https://id.wikipedia.org/wiki/Jambu_biji',
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
