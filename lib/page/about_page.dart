import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        elevation: 10,
        backgroundColor: Color.fromARGB(255, 3, 113, 102),
        title: Text(
          'About',
          style: GoogleFonts.poppins(
              fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(5, 10, 5, 0),
        child: Text(
            'Aplikasi ini dibuat guna untuk menuntaskan tugas mata kuliah menejemen kampus hijau. Aplikasi ini memiliki hak cipta!'),
      ),
    );
  }
}
