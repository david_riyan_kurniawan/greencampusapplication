import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_flutter/qr_flutter.dart';

class BelimbingPage extends StatelessWidget {
  const BelimbingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        elevation: 10,
        backgroundColor: Color.fromARGB(255, 3, 113, 102),
        title: Text(
          'BELIMBiNG',
          style: GoogleFonts.poppins(
              fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white),
        ),
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Text(
                'Belimbing wuluh memiliki batang yang kasar berbenjol-benjol, bercabang sedikit, arahnya condong keatas. Cabang muda berambut halus seperti beludru, warna coklat muda. Daun berupa daun majemuk menyirip ganjil dengan 21-45 pasang anak daun.',
                style: TextStyle(fontSize: 15),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Center(
                  child: QrImage(
                    size: 300,
                    version: 6,
                    backgroundColor: Colors.grey,
                    foregroundColor: Colors.black,
                    errorCorrectionLevel: QrErrorCorrectLevel.M,
                    padding: EdgeInsets.all(30),
                    data: 'https://id.wikipedia.org/wiki/Belimbing_sayur',
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
