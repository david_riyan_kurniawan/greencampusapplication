import 'package:flutter/material.dart';
import 'package:project_mkh/page/about_page.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:project_mkh/page/belimbing_page.dart';
import 'package:project_mkh/page/jambu_page.dart';
import 'package:project_mkh/page/kelengkeng_page.dart';
import 'package:project_mkh/page/mangga_page.dart';
import 'package:project_mkh/page/profil_kelompok.dart';

import 'package:carousel_slider/carousel_slider.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _current = 0;
  final CarouselController _controller = CarouselController();
  //membuat list row
  List<Widget> app = [
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/pic-1.jpeg'), fit: BoxFit.contain)),
      ),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/pic-2.jpeg'), fit: BoxFit.contain)),
      ),
    ),
    ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/pic-3.jpeg'), fit: BoxFit.contain)),
      ),
    )
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 5,
          toolbarHeight: 70,
          title: const Text(
            'GREEN CAMPUS',
            style: TextStyle(fontSize: 23),
          ),
          flexibleSpace: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.teal, Colors.green, Colors.teal],
                    begin: Alignment.centerLeft,
                    end: Alignment.bottomRight)),
          ),
        ),
        //membuat menu drawer
        //yang terdiri sebagai berikut
        drawer: Drawer(
          elevation: 50,
          width: 280,
          child: Column(
            children: [
              Container(
                height: 245,
                color: const Color.fromARGB(255, 3, 161, 146),
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        'Menu Pilihan',
                        style: GoogleFonts.poppins(
                            fontSize: 25,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      )),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              ListTile(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) {
                      return const ProfilKelompok();
                    },
                  ));
                },
                leading: const Icon(
                  Icons.person,
                ),
                title: const Text(
                  'Profile kelompok',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) {
                      return const AboutPage();
                    },
                  ));
                },
                leading: const Icon(Icons.help),
                title: const Text(
                  'About',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                ),
              )
            ],
          ),
        ),
        body: ListView(children: [
          //membuat scrol card kesamping
          Container(
            margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                //membuat list widget
                CarouselSlider(
                  items: app,
                  carouselController: _controller,
                  options: CarouselOptions(
                      autoPlay: true,
                      aspectRatio: 19.7 / 8,
                      enlargeCenterPage: true,
                      onPageChanged: (index, reason) {
                        setState(() {
                          _current = index;
                        });
                      }),
                ),
                //membuat indicatornya
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: app.asMap().entries.map((entry) {
                    return GestureDetector(
                      onTap: () => _controller.animateToPage(entry.key),
                      child: Container(
                        width: 8.0,
                        height: 8.0,
                        margin: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 4.0),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color:
                                (Theme.of(context).brightness == Brightness.dark
                                        ? Colors.white
                                        : Colors.teal)
                                    .withOpacity(
                                        _current == entry.key ? 0.9 : 0.4)),
                      ),
                    );
                  }).toList(),
                ),
                //akhir scrol card kesamping
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        //<---------MEMBUAT CARD PERTAMA--------->
                        Container(
                          margin: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.48,
                            height: MediaQuery.of(context).size.height * 0.3,
                            child: Card(
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              child: Column(
                                children: [
                                  Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(10),
                                            topRight: Radius.circular(10)),
                                        image: DecorationImage(
                                            image:
                                                AssetImage('images/jambu.jpeg'),
                                            fit: BoxFit.cover)),
                                  ),
                                  Text(
                                    'JAMBU',
                                    style: GoogleFonts.poppins(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Material(
                                    borderRadius: BorderRadius.circular(10),
                                    elevation: 5,
                                    child: Container(
                                        margin: const EdgeInsets.fromLTRB(
                                            0, 5, 0, 0),
                                        height: 25,
                                        width: 70,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            gradient: const LinearGradient(
                                                colors: [
                                                  Colors.teal,
                                                  Colors.green
                                                ])),
                                        child: Material(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.transparent,
                                          child: InkWell(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            onTap: () {
                                              Navigator.push(context,
                                                  MaterialPageRoute(
                                                builder: (context) {
                                                  return const JambuPage();
                                                },
                                              ));
                                            },
                                            child: Center(
                                                child: Text(
                                              'Next',
                                              textAlign: TextAlign.center,
                                              style: GoogleFonts.aBeeZee(
                                                  color: Colors.white),
                                            )),
                                          ),
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        //<---------- AKHIR CARD PERTAMA ------->
                        //<---------- CARD KEDUA -------------->
                        Container(
                          margin: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.48,
                            height: MediaQuery.of(context).size.height * 0.3,
                            child: Card(
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              child: Column(
                                children: [
                                  Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(10),
                                            topRight: Radius.circular(10)),
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'images/kelengkeng.jpeg'),
                                            fit: BoxFit.cover)),
                                  ),
                                  Text(
                                    'KELENGKENG',
                                    style: GoogleFonts.poppins(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Material(
                                    borderRadius: BorderRadius.circular(10),
                                    elevation: 5,
                                    child: Container(
                                        margin: const EdgeInsets.fromLTRB(
                                            0, 5, 0, 0),
                                        height: 25,
                                        width: 70,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            gradient: const LinearGradient(
                                                colors: [
                                                  Colors.teal,
                                                  Colors.green
                                                ])),
                                        child: Material(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.transparent,
                                          child: InkWell(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            onTap: () {
                                              Navigator.push(context,
                                                  MaterialPageRoute(
                                                builder: (context) {
                                                  return const KelengkengPage();
                                                },
                                              ));
                                            },
                                            child: Center(
                                                child: Text(
                                              'Next',
                                              textAlign: TextAlign.center,
                                              style: GoogleFonts.aBeeZee(
                                                  color: Colors.white),
                                            )),
                                          ),
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        //<----------- AKHIR CARD KEDUA --------->
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        //<---------MEMBUAT CARD KETIGA--------->
                        Container(
                          margin: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.48,
                            height: MediaQuery.of(context).size.height * 0.3,
                            child: Card(
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              child: Column(
                                children: [
                                  Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(10),
                                            topRight: Radius.circular(10)),
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'images/mangga.jpeg'),
                                            fit: BoxFit.cover)),
                                  ),
                                  Text(
                                    'MANGGA',
                                    style: GoogleFonts.poppins(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Material(
                                    borderRadius: BorderRadius.circular(10),
                                    elevation: 5,
                                    child: Container(
                                        margin: const EdgeInsets.fromLTRB(
                                            0, 5, 0, 0),
                                        height: 25,
                                        width: 70,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            gradient: const LinearGradient(
                                                colors: [
                                                  Colors.teal,
                                                  Colors.green
                                                ])),
                                        child: Material(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.transparent,
                                          child: InkWell(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            onTap: () {
                                              Navigator.push(context,
                                                  MaterialPageRoute(
                                                builder: (context) {
                                                  return const ManggaPage();
                                                },
                                              ));
                                            },
                                            child: Center(
                                                child: Text(
                                              'Next',
                                              textAlign: TextAlign.center,
                                              style: GoogleFonts.aBeeZee(
                                                  color: Colors.white),
                                            )),
                                          ),
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        //<---------- AKHIR CARD KETIGA ------->
                        //<---------- CARD KEEMPAT -------------->
                        Container(
                          margin: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.48,
                            height: MediaQuery.of(context).size.height * 0.3,
                            child: Card(
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              child: Column(
                                children: [
                                  Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(10),
                                            topRight: Radius.circular(10)),
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'images/belimbing.jpeg'),
                                            fit: BoxFit.cover)),
                                  ),
                                  Text(
                                    'BELIMBING',
                                    style: GoogleFonts.poppins(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Material(
                                    borderRadius: BorderRadius.circular(10),
                                    elevation: 5,
                                    child: Container(
                                        margin: const EdgeInsets.fromLTRB(
                                            0, 5, 0, 0),
                                        height: 25,
                                        width: 70,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            gradient: const LinearGradient(
                                                colors: [
                                                  Colors.teal,
                                                  Colors.green
                                                ])),
                                        child: Material(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.transparent,
                                          child: InkWell(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            onTap: () {
                                              Navigator.push(context,
                                                  MaterialPageRoute(
                                                builder: (context) {
                                                  return const BelimbingPage();
                                                },
                                              ));
                                            },
                                            child: Center(
                                                child: Text(
                                              'Next',
                                              textAlign: TextAlign.center,
                                              style: GoogleFonts.aBeeZee(
                                                  color: Colors.white),
                                            )),
                                          ),
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        //<----------- AKHIR CARD KEEMPAT --------->
                      ],
                    ),
                  ],
                )
              ],
            ),
          )
        ]));
  }
}
