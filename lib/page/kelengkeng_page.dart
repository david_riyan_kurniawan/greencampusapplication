import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_flutter/qr_flutter.dart';

class KelengkengPage extends StatelessWidget {
  const KelengkengPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        elevation: 10,
        backgroundColor: Color.fromARGB(255, 3, 113, 102),
        title: Text(
          'KELENGKENG',
          style: GoogleFonts.poppins(
              fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white),
        ),
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Text(
                'Tanaman kelengkeng adalah tanaman berbentuk pohon dengan tinggi bisa mencapai 40 meter dan diameter batang 1 meter. Jenis akar pada tanaman lengkeng adalah akar tunggang dengan akar samping yang berjumlah banyak, panjang dan kuat. Tanaman ini diperkirakan bersal dari daratan cina.',
                style: TextStyle(fontSize: 15),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Center(
                  child: QrImage(
                    size: 300,
                    version: 6,
                    backgroundColor: Colors.grey,
                    foregroundColor: Colors.black,
                    errorCorrectionLevel: QrErrorCorrectLevel.M,
                    padding: EdgeInsets.all(30),
                    data: 'https://id.wikipedia.org/wiki/Lengkeng',
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
